#include <iostream>
#include <iterator>
#include <vector>
#include "DGtal/base/Common.h"
#include "DGtal/base/BasicTypes.h"
#include "DGtal/helpers/StdDefs.h"

//! [LambdaMST2DHeader]
#include "DGtal/geometry/curves/ArithmeticalDSSComputer.h"
#include "DGtal/geometry/curves/SaturatedSegmentation.h"
#include "DGtal/geometry/curves/estimation/LambdaMST2D.h"
#include <DGtal/io/boards/Board2D.h>
//! [LambdaMST2DHeader]


#include <boost/program_options/options_description.hpp>
#include <boost/program_options/parsers.hpp>
#include <boost/program_options/variables_map.hpp>


using namespace std;
using namespace DGtal;
using namespace Z2i;

namespace po = boost::program_options;


int main(int argc, char** argv ){
  
  //! [LambdaMST2DStandardCtor]
  typedef vector < Point > Container;
  typedef Container::const_iterator ConstIterator;
  typedef ArithmeticalDSSComputer < ConstIterator, int, 8 > SegmentComputer;
  typedef SaturatedSegmentation<SegmentComputer> Segmentation;
  LambdaMST2D < Segmentation > lmst;
  //! [LambdaMST2DStandardCtor]
  
  
  
  po::options_description general_opt("Allowed options are: ");
  general_opt.add_options()
  ("help,h", "display this message")
  ("input,i", po::value<std::string>(), "input contour file name");
  
  bool parseOK=true;
  po::variables_map vm;
  try{
    po::store(po::parse_command_line(argc, argv, general_opt), vm);
  }catch(const std::exception& ex){
    parseOK=false;
    trace.info()<< "Error checking program options: "<< ex.what()<< std::endl;
  }
  po::notify(vm);
  if(!parseOK || vm.count("help")||argc<=1 || (!(vm.count("input"))) )
  {
    trace.info()<< "Test LMST " <<std::endl << "Basic usage: "<<std::endl
    << "\t  [options] --input  <fileName> "<<std::endl
    << general_opt << "\n";
    return 0;
  }
  
  
  
  // Input points
  Container contour;
  contour.push_back(Point(18,18));
  contour.push_back(Point(17,19));
  contour.push_back(Point(16,20));
  contour.push_back(Point(15,21));
  contour.push_back(Point(14,22));
  contour.push_back(Point(13,23));
  contour.push_back(Point(12,24));
  contour.push_back(Point(11,25));
  contour.push_back(Point(10,26));
  contour.push_back(Point(9,27));
  contour.push_back(Point(8,28));
  
  
  Board2D aBoard;
  for(auto const &p: contour)
  {
    aBoard << p;
  }
  
  //! [LambdaMST2DTangential]
  // Initialization of tangential cover
  Segmentation segmenter ( contour.begin(), contour.end(), SegmentComputer() );
  lmst.attach ( segmenter );
  //! [LambdaMST2DTangential]
  
  //! [LambdaMST2DPoint]
  for ( ConstIterator it = contour.begin(); it != contour.end(); ++it )
    lmst.eval ( it );
  //! [LambdaMST2DPoint]
  
  //! [LambdaMST2DFast]
  lmst.init ( contour.begin(), contour.end() );
  std::vector < RealVector > tangent;
  lmst.eval < back_insert_iterator< vector < RealVector > > > ( contour.begin(), contour.end(),  back_inserter ( tangent ) );
  //! [LambdaMST2DFast]
  aBoard.saveEPS("res.eps");
  
  return 1;
}
